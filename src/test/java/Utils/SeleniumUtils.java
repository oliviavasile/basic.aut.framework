package Utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumUtils {
    WebDriver driver;
    Browsers browsers = null;


    public static WebDriver getDriver (String browserType) {
        WebDriver driver = null;
        Browsers browsers = getBrowserEnumFromString(browserType);


        switch (browsers) {
            case CHROME:
                WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
                driver = new ChromeDriver();
                break;
            case FIREFOX:
                WebDriverManager.getInstance(DriverManagerType.FIREFOX).setup();
                driver = new FirefoxDriver();
                break;
            case EDGE:
                WebDriverManager.getInstance(DriverManagerType.EDGE).setup();
                driver = new EdgeDriver();
                break;
            default:
                System.out.println("Selected driver is not supported" + browsers.toString());

        }
        return driver;
    }

//     if(browserType.equalsIgnoreCase(Browsers.CHROME.toString())){
//           WebDriverManager.getInstance(DriverManagerType.CHROME).setup();driver = new ChromeDriver();
//     }


    public static void getBrowserEnumFromString (String browserType) {
        for (Browsers browser : Browsers.values()) {
            if (browserType.equalsIgnoreCase(Browsers.CHROME.toString()))
                return browser;
            System.out.println("Browser not found");}
            return null;

        }
    }
}



